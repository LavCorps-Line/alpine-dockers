# LavCorps-Line Alpine Dockers

Starting with v3.20, container images support x86_64 and aarch64 architecture. 

---

To use the latest stable image, use:

* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.21:stable`
    * This image contains Alpine Linux, v3.21
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.20:stable`
    * This image contains Alpine Linux, v3.20
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.19:stable`
    * This image contains Alpine Linux, v3.19
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.18:stable`
    * This image contains Alpine Linux, v3.18

Stable images are not built for Edge or Edge + testing, due to their nature as
less stable iterations of Alpine Linux.

The latest stable image will be periodically regenerated for the latest updates.

---

To use the latest (unstable!) image, use:

* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/edgetesting:latest`
    * This image contains Alpine Linux on Edge + Testing repos
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/edge:latest`
    * This image contains Alpine Linux on Edge
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.21:latest`
    * This image contains Alpine Linux, v3.21
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.20:latest`
    * This image contains Alpine Linux, v3.20
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.19:latest`
    * This image contains Alpine Linux, v3.19
* `registry.gitlab.com/lavcorps-line/infrastructure/alpine-dockers/3.18:latest`
    * This image contains Alpine Linux, v3.18

Unstable images are generated once, upon commit.
