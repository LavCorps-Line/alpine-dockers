let GitLab = ./GitLab.dhall

let Prelude = ./Prelude.dhall

let JobType = ./JobType.dhall

let MkJobRecord = ./MkJobRecord.dhall

let Target = ./Target.dhall

let Targets = ./Targets.dhall

let Jobs
    : List { mapKey : Text, mapValue : GitLab.Job.Type }
    =   Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.latest True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.stable True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.scheduled True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.latest False)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.stable False)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          GitLab.Job.Type
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional GitLab.Job.Type }
              (MkJobRecord JobType.scheduled False)
              Targets
          )

in  Jobs
