let GitLab = ./GitLab.dhall

let Prelude = ./Prelude.dhall

let Target = ./Target.dhall

let JobType = ./JobType.dhall

let MkJob = ./MkJob.dhall

let MkJobRecord
    : forall (type : Natural) ->
      forall (build : Bool) ->
      forall (target : Target) ->
        { mapKey : Text, mapValue : Optional GitLab.Job.Type }
    = \(type : Natural) ->
      \(build : Bool) ->
      \(target : Target) ->
        if    target.stable
        then  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = Some (MkJob type build target)
              }
        else  if Prelude.Natural.equal type JobType.latest
        then  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = Some (MkJob type build target)
              }
        else  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = None GitLab.Job.Type
              }

in  MkJobRecord
